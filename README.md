# RUFLIX
_[Rutracker.org] client_

Cross-platform (Windows/Linux/Mac) torrent-media streamer, providing immediate access to music and video content shared by torrents on rutracker.org.

Inspired by [Butter-project], based on [Webtorrent]
****
Ruflix is in active development right now, and first (alpha) versions are planned for Nov. 2017.

**This package is builder**, dependend on [backend-rustreamer] (node-part of Ruflix)
d [ui-rustreamer] (AngularJS part of Ruflix)

****
**Features**

Already works:
- Read Rutracker catalogs and forums provided by api.rutracker.org
- Read Rutracker feed (feed.rutracker.org)
- Playing audio
- torrent stream/host (backend)
- Video player (ui)

In active development:
- Player optimizations and functionalities

Planned:
- E-book reader
- I2P connection support (anonimization and censorship bypass)
- UPnP and NAT-PMP port forwarding
- Caching downloads, playing statistics
- Mobile (Cordova) version
- CI, tests, other dev-ops
- many other features

****

**Requiremenets**

- node.js version min. 6.31
- Google Chrome (for hosting dev server)

****

**Install**

- On Linux install xvfb `sudo apt-get install xvfb`
- `npm install -g yarn`
- `git glone git@gitlab.com:thecyberd3m0n/Ruflix.git`
- `yarn install`

****

**Build or run**

- Run dev server `gulp serve` and type url `http://localhost:7000` to Google Chrome
- Build `gulp build --platforms=platform` (where platform is one of win32 [Windows 32-bit], win64 [Windows 64-bit], linux64 [Linux 64-bit])
build folder will be created with your native builds.

****

**Develop and contribute**

You can run the Ruflix in browser for development purposes, 
but browsers like Chrome don't contain some useful codecs, so some media will not work.
That's why we're using custom Chromium build, provided also by [butter-project] developers (I owe you a vodka:))
So,
- git clone projects [backend-rustreamer], [ui-rustreamer], and [Ruflix] to same directory
- `npm install -g yarn`
- `cd backend-rustreamer && npm link && cd ..`
- `cd ui-rustreamer && npm link && cd ..`
- `cd Ruflix && yarn install && npm link backend-rustreamer && npm link ui-rustreamer`
- `gulp serve` for Chrome-based development
- `npm unlink backend-rustreamer && npm unlink ui-rustreamer && gulp build` for builds. 
Unfortunately, build fails if it will find git repos right now, it will be fixed in the future.

****

**License**

GPL v3 license.

[Butter-project]: http://butterproject.org/

[backend-rustreamer]: https://gitlab.com/thecyberd3m0n/backend-rustreamer
[ui-rustreamer]: https://gitlab.com/thecyberd3m0n/ui-rustreamer
[Ruflix]: https://gitlab.com/thecyberd3m0n/Ruflix
[Webtorrent]: https://webtorrent.io/#
[Rutracker.org]: http://rutracker.org/